#!/usr/bin/python3
"""Print out a list of .com domain name using numbers.

Change SIZE constant to how many domain names you want
"""

SIZE = 1000

for i in range(0, SIZE):
    print(str(i) + ".com")
