#!/usr/bin/python3
# Note for the next python programmer:
# - all the non-test script follows the same format
# - if you are creating a new script, do the following step:
#   1. copy this script and the test script into two new files
#   2. on both script replace all "resolve_ns" with your file name (without ".py")
#   3. replace the code in test_domain function
#   4. test your script by calling `python3 unittest -m resolve_ns_test.py`
#      (replace `resolve_ns_test.py` with your file name)

""" Resolve domain name by name server record and compare it to VCN's

Finds the name server(s) by either command arguments or by file pipe in. Then
use the name server(s) check against VCN a list of servers.

"""
import socket
import os
import sys
import dns
import dns.query
import dns.resolver

def test_domain(domain_name):
	""" Print out domain names that caused an error or not hosted by VCN

	Argument:
		domain_name -- the domain name to check
	"""
	VCN_DOMAINS = "vcn.bc.ca.", "vancouvercommunity.net."
	try:
		# socket.gethostbyname(domain_name)
		answers = dns.resolver.query(domain_name, 'NS')
		notFound = True
		for ns in answers:
			compare = ns.to_text()
			for domain in VCN_DOMAINS:
				if compare.endswith(domain):
					notFound = False

		if notFound:
			print("Not in VCN name server:  " + domain_name)
	except dns.exception.Timeout:
		print("DNS operation timeout:   " + domain_name)
	except dns.resolver.NXDOMAIN:
		print("Nonexisting query name:  " + domain_name)
	except dns.resolver.NoAnswer:
		print("Contains no answer:      " + domain_name);
	except dns.resolver.NoNameservers:
		print("No name server replies:  " + domain_name)

def main(argv):
	""" The main method of this script

	This method will be called if this script is called from terminal

	Argument:
		argv -- program arguments
	"""
	usage = True
	if not sys.stdin.isatty():
		for line in sys.stdin.readlines():
			test_domain(line.strip())
		usage = False
	if len(argv) >= 1:
		for arg in argv:
			test_domain(arg)
		usage = False
	if usage:
		print("Usage: python resolve_ns \"domain name\"")
		print("Usage: cat \"text file path\" | python resolve_ns")

if __name__ == '__main__':
	main(sys.argv[1:])
