domain-check
============

This project depends on the package dnspython. Install by calling:

```bash
pip3 install  dnspython
```

It uses python 3.

## Working Scripts

- `resolve_ip.py` filter out websites that are using VCN IP.
- `resolve_ns.py` filter out websites that are using VCN name severs.


## Running the script
They can be use from a file pipping standard in like the following:

```bash
cat ${file} | ./resolve_ip.py
cat ${file} | ./resolve_ns.py
```

They can be run to check the domain name in the arguments like the following:

```bash
./resolve_ip.py ${website_1} ${website_2} ... ${website_last}
./resolve_ns.py ${website_1} ${website_2} ... ${website_last}
```

They can also be import into another python script and call `test_domain` function as the
following:

```python
import resolve_ip
import resolve_ns

resolve_ip.test_domain("website_domain_name")
resolve_ns.test_domain("website_domain_name")
```

## Testing scripts

To test the scripts runs:

```bash
python3 -m unittest resolve_ip.py
```

The above test with detail

```bash
python3 -m unittest -v resolve_ns.py
```

To stress test this script, run:

```bash
time python build_files.py | python3 resolve_ns.py
```
