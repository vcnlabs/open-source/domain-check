#!/usr/bin/python3
# Note for the next python programmer:
# - all the non-test script follows the same format
# - if you are creating a new script, do the following step:
#   1. copy this script and the test script into two new files
#   2. on both script replace all "resolve_ip" with your file name (without ".py")
#   3. replace the code in test_domain function
#   4. test your script by calling `python3 unittest -m  resolve_ip_test.py`
#      (replace `resolve_ip_test.py` with your file name)

""" Resolve domain name by IP address and compare it to VCN's

Finds the IP address(es) by either command arguments or by file pipe in. Then
use the IP address(es) check against VCN's IP.

"""
import socket
import os
import sys


def test_domain(domain_name):
	""" Print out domain names that caused an error or not hosted by VCN

	Argument:
		domain_name -- the domain name to check
	"""
	VCN_IP = "207.102.64."
	try:
		ip = socket.gethostbyname(domain_name)
		if not ip.startswith(VCN_IP):
			print(domain_name)
	except socket.gaierror:
		print(domain_name)

def main(argv):
	""" The main method of this script

	This method will be called if this script is called from terminal

	Argument:
		argv -- program arguments
	"""
	usage = True
	if not sys.stdin.isatty():
		for line in sys.stdin.readlines():
			test_domain(line.strip())
		usage = False
	if len(argv) >= 1:
		for arg in argv:
			test_domain(arg)
		usage = False
	if usage:
		print("Usage: python resolve_ip \"domain name\"")
		print("Usage: cat \"text file path\" | python resolve_ip")

if __name__ == '__main__':
	main(sys.argv[1:])
