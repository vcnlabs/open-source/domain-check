#!/usr/bin/python3
# Note for the next python programmer:
# - the tests are the function in ResolveTests classes
# - frist string after def is the input
# - second string after def is the expected output

""" Unit Test for resolve_ip.py

This script following the examples on https://stackoverflow.com/a/39908502 and
on https://docs.python.org/3/library/unittest.mock.html (search sys.stdout)
"""
import unittest
import resolve_ip
from unittest.mock import patch
import sys
from io import StringIO

class ResolveTests(unittest.TestCase):
    """Tests related to resolve_ip.py

    This Test Unit uses unittest.mock which requires python 3
    """
    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_google(self, mock_stdout):
        """Test known domain name that is not hosted on VCN

        """
        resolve_ip.test_domain("google.com")
        self.assertEqual(mock_stdout.getvalue(), "google.com\n")


    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_google(self, mock_stdout):
        """Test known domain name that is hosted on VCN and does not ends with
            `vcn.bc.ca`

        """
        resolve_ip.test_domain("avalonrecoverysociety.org")
        self.assertEqual(mock_stdout.getvalue(), "")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_vcn(self, mock_stdout):
        """Test known domain name that is hosted on VCN
        """
        resolve_ip.test_domain("vcn.bc.ca")
        self.assertEqual(mock_stdout.getvalue(), "")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_123(self, mock_stdout):
        """Test a number and make sure nothing breaks
        """
        resolve_ip.test_domain("123")
        self.assertEqual(mock_stdout.getvalue(), "123\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_non_exisiting(self, mock_stdout):
        """Test a domain name that does not exist
        """
        resolve_ip.test_domain("1234567dasfsdfaaewlfodg.com")
        self.assertEqual(mock_stdout.getvalue(), "1234567dasfsdfaaewlfodg.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_empty(self, mock_stdout):
        """Test a string with no characters
        """
        resolve_ip.test_domain("")
        self.assertEqual(mock_stdout.getvalue(), "\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_google_domain(self, mock_stdout):
        """Test a subdomain of google.com
        """
        resolve_ip.test_domain("mail.google.com")
        self.assertEqual(mock_stdout.getvalue(), "mail.google.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_vcn_domain(self, mock_stdout):
        """Test a subdomain of vcn.bc.ca
        """
        resolve_ip.test_domain("fexlix.vcn.bc.ca")
        self.assertEqual(mock_stdout.getvalue(), "")

    @patch('sys.stdout', new_callable=StringIO)
    def test_resolve_empty(self, mock_stdout):
        """Test a string with no characters
        """
        resolve_ip.test_domain("")
        self.assertEqual(mock_stdout.getvalue(), "\n")

    # Not testing max string with max length b/c it would take all the memory

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_usage(self, mock_stdout):
        """Test to show script usage
        """
        resolve_ip.main([])
        self.assertEqual(mock_stdout.getvalue(),
            "Usage: python resolve_ip \"domain name\"\n" +
	        "Usage: cat \"text file path\" | python resolve_ip\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_single_arg(self, mock_stdout):
        """Test using one argument
        """
        resolve_ip.main(["google.com"])
        self.assertEqual(mock_stdout.getvalue(), "google.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_option_two_args(self, mock_stdout):
        """Test using two arguments
        """
        resolve_ip.main(["google.com", "apple.com"])
        self.assertEqual(mock_stdout.getvalue(), "google.com\napple.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    @patch("sys.stdin", StringIO("google.com"))
    def test_main_single_line(self, mock_stdout):
        """Test using file in with one line
        """
        resolve_ip.main([])
        self.assertEqual(mock_stdout.getvalue(), "google.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    @patch("sys.stdin", StringIO("google.com\nyahoo.com"))
    def test_main_two_line(self, mock_stdout):
        """Test using file in with two line
        """
        resolve_ip.main([])
        self.assertEqual(mock_stdout.getvalue(), "google.com\nyahoo.com\n")

    @patch('sys.stdout', new_callable=StringIO)
    @patch("sys.stdin", StringIO("google.com"))
    def test_main_args_and_lines(self, mock_stdout):
        """Test using both files in and arguments
        """
        resolve_ip.main(["youtube.com"])
        self.assertEqual(mock_stdout.getvalue(), "google.com\nyoutube.com\n")

    # Not testing max arguments b/c it is limited by OS


if __name__ == '__main__':
    unittest.main();
